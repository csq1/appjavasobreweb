package Practica_1.FlujoIO;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FlujoIO {
    public static void main(String[] args) throws IOException {
        String in, out;

        //LeerTeclado leerTeclado = new LeerTeclado();
        //LeerTecladoError leerTecladoError = new LeerTecladoError();
        //LectorSaveTxt lectorSaveTxt = new LectorSaveTxt();
        //LectorSaveToXML lectorSaveToXML = new LectorSaveToXML();

        LeerTecladoThread leerTecladoThread;
        System.out.println("Escribir msj para enviar via Thread: ");
        in = new Scanner(System.in).nextLine();
        out = "I'm back: " + in;

        leerTecladoThread = new LeerTecladoThread(in, out);
        leerTecladoThread.start();
    }
}

//  Redirecciona el flujo de datos de una entrada (teclado) a una salida por pantalla
class LeerTeclado {
    int c;

    public LeerTeclado() throws IOException {
        System.out.println("Escribir y finalizar con Enter:");

        while( (c = System.in.read()) != '\n' ){
            System.out.print( (char) c );
        }
    }
}

//  Redirecciona el flujo de datos de una entrada (teclado) a una salida tipo error
class LeerTecladoError {
    int c;

    public LeerTecladoError() throws IOException {
        System.out.println("Escribir y finalizar con Enter:");

        while( (c = System.in.read()) != '\n' ){
            System.err.print( (char) c );
        }
    }
}

// Lector de caracteres tipeados, con guardado en un archivo tipo texto
class LectorSaveTxt {
    int c;

    public LectorSaveTxt() throws IOException {

        // Ruta al archivo a crear
        String path = "C:\\Users\\Carlos\\Documents\\Material\\Aplicaciones Java Sobre Web\\Clases\\Clase 2\\SaveTxt.txt";

        try {
            // Crear objeto y abrir stream
            FileWriter fw = new FileWriter(path);

            // Leer teclado hasta tipear Enter
            System.out.println("Escribir y finalizar con Enter:");

            while( (c = System.in.read()) != '\n' ){
                // Escribir en archivo
                fw.write(c);
            }

            // Guardar cambios
            fw.flush();

            // Cerrar stream
            fw.close();

        }catch (IOException e) {
            System.out.println("No se puede crear archivo en la ruta especificada. Error: "+e);
        }finally {
            System.out.println("Fin del programa");
        }
    }
}

// Similar a LectorSaveTxt pero guardando los datos ingresados en un archivo xml
class LectorSaveToXML {

    public LectorSaveToXML() throws IOException {

        // Ruta al archivo a crear
        String path = "C:\\Users\\Carlos\\Documents\\Material\\Aplicaciones Java Sobre Web\\Clases\\Clase 2\\Save.xml";

        try {
            // Crear objeto y abrir stream
            FileWriter fw = new FileWriter(path);

            // Leer teclado hasta tipear FIN
            System.out.println("Escribir y finalizar con FIN:");

            //  Scanner para la lectura de datos
            Scanner reader = new Scanner(System.in);
            String texto = reader.next();

            // Header
            fw.write("<header>");

            while (!texto.equals("FIN")) {

                // Escribir en archivo
                fw.write(System.lineSeparator());
                fw.write("<line>" + texto + "</line>");

                texto = reader.next();
            }

            // Header
            fw.write(System.lineSeparator());
            fw.write("</header>");

            // Guardar cambios
            fw.flush();

            // Cerrar stream
            fw.close();

        } catch (IOException e) {
            System.out.println("No se puede crear archivo en la ruta especificada. Error: " + e);
        } finally {
            System.out.println("Fin del programa");
        }
    }
}

// Crear el punto 1 con un thread, pasando el in y out
class LeerTecladoThread extends Thread{
    String in, out;

    public LeerTecladoThread(String in, String out) {
        this.in = in;
        this.out = out;
    }

    public void run() {
        System.out.println("I am a Thread!, your msj in: " + this.in + " your msj out: " + this.out);
    }
}