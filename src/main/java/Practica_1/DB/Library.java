package Practica_1.DB;

import java.sql.*;

public class Library {
    Connector connectDB = new Connector();
    Connection connection = connectDB.MySQLConnection();

    public void AddBook(Book book){
        try {
            PreparedStatement pst = connection.prepareStatement(
                    "INSERT INTO books (title, autor, reservation, isbn) VALUES (?, ?, ?, ?)");
            pst.setString(1, book.getTitle());
            pst.setString(2,  book.getAutor());
            pst.setNull(3, java.sql.Types.INTEGER);
            pst.setString(4,  book.getIsbn());
            pst.execute();

            System.out.println("Database insert successfully");
        }catch (SQLException e) {
            System.out.println("Error: " + e);
        }
    }

    public void Reservation(String user, Book book) {
        try {
            PreparedStatement pst = connection.prepareStatement(
                    "UPDATE books SET reservation = ? WHERE isbn = ?");
            pst.setString(1, user);
            pst.setString(2, book.getIsbn());
            pst.executeUpdate();

            System.out.println("Database updated successfully");
        }catch (SQLException e) {
            System.out.println("Error: " + e);
        }
    }

    public void ListReservation() {
        String query = "SELECT title, reservation FROM books WHERE reservation IS NOT NULL";
        System.out.println("N° - Title - Reservation");
        try {
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(query);

            int count = 1;
            while (rs.next())
            {
                String title = rs.getString("title");
                String reservation = rs.getString("reservation");

                // print the results
                System.out.format("%s - %s - %s\n", count, title, reservation);
                count++;
            }
            st.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
