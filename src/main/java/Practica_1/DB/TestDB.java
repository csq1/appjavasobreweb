package Practica_1.DB;

import java.util.ArrayList;

public class TestDB {
    public static void main(String[] args) {
        Book b1 = new Book("Cien años de soledad","García Márquez, Gabriel","978-950-614-0113");
        Book b2 = new Book("Cuentos completos", "Poe, Edgar Alla", "978-987-3952-53-1");
        Book b3 = new Book("Cuentos de la selva", "Quiroga, Horacio", "978-987-613-186-5");
        Book b4 = new Book("Cuentos completos 1", "Cortázar, Julio", "978-987-725-371-9");

        ArrayList<Book> bookArray = new ArrayList<Book>();
        bookArray.add(b1);
        bookArray.add(b2);
        bookArray.add(b3);
        bookArray.add(b4);

        Library library = new Library();

        for (int i = 0; i <bookArray.size(); i++) {
            library.AddBook(bookArray.get(i));
        }

        System.out.println("--------------------------------------------------------------------------------------");
        library.Reservation("Pablo, Rodriguez", b1);
        library.Reservation("Lopez, Julio", b3);

        System.out.println("--------------------------------------------------------------------------------------");
        library.ListReservation();
    }
}