package Practica_1.Matrices;

public class TestMatrices {
    public static void main(String[] args){
        CalculadoraDeMatrices calculadoraDeMatrices = new CalculadoraDeMatrices();

        int[][] m1 = {{3,1,1},{2,7,0},{11,11,1}};
        int[][] m2 = {{4,2,1},{5,7,2},{2,2,3}};

        System.out.println("--------------------Matriz A---------------------");

        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                System.out.print("["+m1[i][j]+"]");
            }
            System.out.println("");
        }
        System.out.println("--------------------Matriz B---------------------");

        for (int i = 0; i < m2.length; i++) {
            for (int j = 0; j < m2[0].length; j++) {
                System.out.print("["+m2[i][j]+"]");
            }
            System.out.println();
        }

        System.out.println("--------------------Suma-------------------------");
        int [][] mSuma = calculadoraDeMatrices.Sumar(m1, m2);

        try {
            for (int i = 0; i < mSuma.length; i++) {
                for (int j = 0; j < mSuma[0].length; j++) {
                    System.out.print("["+mSuma[i][j]+"]");
                }
                System.out.println();
            }
        }catch (Exception e) {
            System.out.println("No values");
        }

        System.out.println("--------------------Resta-------------------------");
        int [][] mResta = calculadoraDeMatrices.Restar(m1, m2);

        try {
            for (int i = 0; i < mResta.length; i++) {
                for (int j = 0; j < mResta[0].length; j++) {
                    System.out.print("["+mResta[i][j]+"]");
                }
                System.out.println();
            }
        }catch (Exception e) {
            System.out.println("No values");
        }

        System.out.println("-------------------------Traza A---------------------------");
        int traza = calculadoraDeMatrices.Trazar(m1);
        System.out.println(traza);

        System.out.println("--------------------Determinante B--------------------------");
        int determinante = calculadoraDeMatrices.Determinante(m2);
        System.out.println(determinante);

        System.out.println("--------------------CheckSquareMatriz AyB--------------------------");
        System.out.println(calculadoraDeMatrices.CheckSquareMatriz(m1,m2));
    }
}
