package Practica_1.Matrices;

public class NumeroEntero extends Numero {
    private int num;

    public NumeroEntero(int n) {
        num = n;
    }

    @Override
    Numero sumar(Numero n) {
        num = num + Integer.parseInt(n.toString());
        return this;
    }

    @Override
    Numero restar(Numero n) {
        num = num - Integer.parseInt(n.toString());
        return this;
    }

    @Override
    Numero multiplicar(Numero n) {
        num = num * Integer.parseInt(n.toString());
        return this;
    }

    @Override
    Numero dividirPor(Numero n) {
        num = num / Integer.parseInt(n.toString());
        return this;
    }

    @Override
    public String toString() {
        return Integer.toString(num);
    }
}