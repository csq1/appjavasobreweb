package Practica_1.Matrices;

public class TestNumeros {
    public static void main(String[] args){
        System.out.println("Numero Entero");
        Numero num = new NumeroEntero(1);
        System.out.println("Numero actual: " + num.toString());
        num.sumar(new NumeroEntero(1));
        System.out.println("Resultado sumar por 1: " + num.toString());

        num.restar(new NumeroEntero(3));
        System.out.println("Resultado restar por 3: " + num.toString());

        num.multiplicar(new NumeroEntero(10));
        System.out.println("Resultado multiplicar por 10: " + num.toString());

        num.dividirPor(new NumeroEntero(2));
        System.out.println("Resultado dividir por 2: " + num.toString());

        System.out.println("--------------------------------------------------------------");
        System.out.println("Numero Real");
        Numero numeroReal = new NumeroReal(120.0);
        System.out.println("Numero actual: " + numeroReal.toString());

        numeroReal.sumar(new NumeroReal(20.0));
        System.out.println("Resultado sumar 20: " + numeroReal.toString());

        numeroReal.restar(new NumeroReal(20.0));
        System.out.println("Resultado restar 20: " + numeroReal.toString());

        numeroReal.multiplicar(new NumeroReal(10.0));
        System.out.println("Resultado multiplicar 10: " + numeroReal.toString());

        numeroReal.dividirPor(new NumeroReal(5.3));
        System.out.println("Resultado dividir 5.3: " + numeroReal.toString());

        System.out.println("--------------------------------------------------------------");
        System.out.println("Numero Complejo");
        Numero numeroComplejo = new NumeroComplejo(12, 10);
        System.out.println("Numero actual: " + numeroComplejo.toString());

        numeroComplejo.sumar(new NumeroComplejo(10, 5));
        System.out.println("Resultado  suma 10 + 5i: " + numeroComplejo.toString());

        numeroComplejo.restar(new NumeroComplejo(5, 2));
        System.out.println("Resultado restar 5 + 2i: " + numeroComplejo.toString());

        numeroComplejo.multiplicar(new NumeroComplejo(2, 5));
        System.out.println("Resultado multiplicar 2 + 5i: " + numeroComplejo.toString());

        numeroComplejo.dividirPor(new NumeroComplejo(3, 2));
        System.out.println("Resultado dividir 3 + 2i: " + numeroComplejo.toString());

    }
}
