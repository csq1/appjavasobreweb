package Practica_1.Matrices;

/* Operaciones con matrices: sumar, restar, traza (suma diagonal) y determinante (producto cruzado a-b).
  Debe permitir operar con matrices de dos dimensiones, de cualquier tamaño */

public class CalculadoraDeMatrices {
    private int[][] result;

    public CalculadoraDeMatrices(){}

    // Sumar solo si M1 y M2 tienen la misma cantidad de filas y columnas
    public int[][] Sumar(int[][] m1, int[][] m2) {
        if (CheckSquareMatriz(m1, m2)) {
            result = new int[m1.length][m1[0].length];

            // Recorrer matriz fila/columna
            for (int i = 0; i < m1.length; i++) {
                for (int j = 0; j < m1[0].length; j++) {
                    result[i][j] = m1[i][j] + m2[i][j];
                }
            }
            return result;
        }
        return result;
    }

    public int[][] Restar(int[][] m1, int[][] m2) {
        if (CheckSquareMatriz(m1, m2)) {
            result = new int[m1.length][m1[0].length];

            // Recorrer fila/columna
            for (int i = 0; i < m1.length; i++) {
                for (int j = 0; j < m1[0].length; j++) {
                    result[i][j] = m1[i][j] - m2[i][j];
                }
            }
            return result;
        }
        return result;
    }

    // Traza suma diagonal - solo si M1 es cuadrada de orden n
    public int Trazar(int[][] m1) {
        int resultado = 0;

        if (CheckSquareMatriz(m1, m1)) {
            // Recorrer fila/columna
            for (int i = 0; i < m1.length; i++) {
                resultado = resultado + m1[i][i];
            }
            return resultado;
        }
        return resultado;
    }

    // Determinante producto cruzado - solo si M1 es cuadrada de orden n (M1 3x3 regla de Sarrus)
    public int Determinante(int[][] m1) {
        int fila = m1.length;
        int col = m1[0].length;
        int determinante;

        // Matriz 1x1
        if ((fila & col) == 1) {
            determinante = m1[0][0];

            // Matriz 2x2
        }else if (((fila & col) == 2)) {
            determinante = (m1[0][0] * m1[1][1]) - (m1[0][1] * m1[1][0]);

            // Matriz 3x3 (Regla de Sarrus)
        }else {
            determinante = ((m1[0][0] * m1[1][1] * m1[2][2]) + (m1[0][1] * m1[1][2] * m1[2][1]) + (m1[1][0] * m1[2][1] * m1[0][2]) ) - ( (m1[2][0] * m1[1][1] * m1[0][2]) + (m1[1][0] * m1[0][1] * m1[2][2]) + (m1[2][1] * m1[1][2] * m1[0][0]) );
        }
        return determinante;

    }

    // True si M1 y M2 tienen la misma cantidad de filas / columnas
    public boolean CheckSquareMatriz(int[][] m1, int[][] m2) {
        int filas_m1 = m1.length;
        int columnas_m1 = m1[0].length;

        int filas_m2 = m2.length;
        int columnas_m2 = m2[0].length;

        return (filas_m1 == filas_m2) & (columnas_m1 == columnas_m2);
    }
}
