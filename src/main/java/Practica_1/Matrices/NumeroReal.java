package Practica_1.Matrices;

public class NumeroReal extends Numero{
    private double num;

    public NumeroReal(Double n) {
        num = n;
    }

    @Override
    Numero sumar(Numero n) {
        num = num + Double.parseDouble(n.toString());
        return this;
    }

    @Override
    Numero restar(Numero n) {
        num = num - Double.parseDouble(n.toString());
        return this;
    }

    @Override
    Numero multiplicar(Numero n) {
        num = num * Double.parseDouble(n.toString());
        return this;
    }

    @Override
    Numero dividirPor(Numero n) {
        num = num / Double.parseDouble(n.toString());
        return this;
    }

    @Override
    public String toString() {
        return Double.toString(num);
    }
}
