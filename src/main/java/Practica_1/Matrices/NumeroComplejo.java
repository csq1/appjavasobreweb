package Practica_1.Matrices;

public class NumeroComplejo extends Numero{
    private double real, img;

    public NumeroComplejo(double real, double img) {
        this.real = real;
        this.img = img;
    }

    @Override
    Numero sumar(Numero n) {
        String[] parts = n.toString().replace("i", "").split("\\+");
        double n_real = Double.parseDouble(parts[0]);
        double n_img = Double.parseDouble(parts[1]);

        real = real + n_real;
        img = img + n_img;

        return this;
    }

    @Override
    Numero restar(Numero n) {
        String[] parts = n.toString().replace("i", "").split("\\+");
        double n_real = Double.parseDouble(parts[0]);
        double n_img = Double.parseDouble(parts[1]);

        real = real - n_real;
        img = img - n_img;

        return this;
    }

    @Override
    Numero multiplicar(Numero n) {
        String[] parts = n.toString().replace("i", "").split("\\+");
        double n_real = Double.parseDouble(parts[0]);
        double n_img = Double.parseDouble(parts[1]);

        double aux = real;
        real = (real * n_real) - (img * n_img);
        img = (aux * n_img) + (img * n_real);

        return this;
    }

    @Override
    Numero dividirPor(Numero n) {
        String[] parts = n.toString().replace("i", "").split("\\+");
        double n_real = Double.parseDouble(parts[0]);
        double n_img = Double.parseDouble(parts[1]);

        double denominador = (Math.pow(n_real, 2)) + (Math.pow(n_img, 2));
        double aux = real;

        real = (((real * n_real)+(img * n_img)) / denominador);
        img = (((img * n_real) - (aux * n_img)) / denominador);

        return this;
    }

    @Override
    public String toString() {
        return real + "+" + img + "i";
    }
}