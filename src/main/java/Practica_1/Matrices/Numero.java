package Practica_1.Matrices;

public abstract class Numero {
    abstract Numero sumar(Numero n);
    abstract Numero restar(Numero n);
    abstract Numero multiplicar(Numero n);
    abstract Numero dividirPor(Numero n);
    public abstract String toString();
}
