package Practica_1.Socket;

import java.io.*;
import java.net.*;
import java.util.Scanner;

class ClientSocket {
    Socket socket = null;
    PrintWriter printWriter;
    DataInputStream inputStream;
    String inputData;
    String host;

    public void init(){
        try{
            // Datos de conexion IP y puerto
            host = "127.0.0.1";
            socket = new Socket(host,15000);

            // Escribir texto e enviarlo en stream
            printWriter = new PrintWriter(socket.getOutputStream(),true);

            // Leer datos de entrada
            inputStream = new DataInputStream(socket.getInputStream());

            // Iniciar la captura del teclado
            Scanner scanner = new Scanner(System.in);
            String read = "";

            // Leer teclado y enviar msj siempre que haya algo que enviar. Terminar loop si se escribe: FIN
            do{
                read = scanner.nextLine();
                if(!read.equals("")) {
                    sendMessage(read);
                }
            }while(!read.equals("FIN"));
        }catch (IOException e){
            System.err.println("Problemas");
        }
    }

    // Metodo de finalizacion cierra inputStream, printWrite y socket
    public void stop(){
        try{
            inputStream.close();
            printWriter.close();
            socket.close();
        }catch (IOException e){
        }
    }

    // Recibe un String e imprime el mismo
    public void sendMessage(String message){
        try{
            printWriter.println(message);
            int l = inputStream.readInt();
        }catch (IOException e){
        }
    }
}

// Instancia un objeto ClienteSocket e inicia la conexion
class Client{
    public static void main(String[] args){
        ClientSocket clientSocket = new ClientSocket();
        clientSocket.init();
    }
}

/*
La clase Client contiene el main donde instancia un ClientSocket e inicia la ejecucion del mismo. ClientSocket presenta
los datos, IP y puerto, para conectarse al socket del servidor. Y posee los sentencias printWriter que convierte el flujo
de datos de la entrada estandar en un Stream para ser enviado al servidor, para esto se inicia el Scanner para capturar
el teclado, cuando se oprime Enter se llama al metodo sendMessage que imprime el msj a enviar.
Tambien se cuenta el metodo stop que se encarga se cerrar el inputStream, printWriter y el socket estalbecido
*/