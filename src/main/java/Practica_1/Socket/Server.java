package Practica_1.Socket;

import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args){
        ServerSocket serverSocket = null; // Socket del servidor
        Socket client = null; // Socket del cliente

        boolean listen = true;
        try{
            // Abrir Socket en el puerto 15000 e imprimir msj
            serverSocket = new ServerSocket(15000);
            System.out.println("Practica_1.Socket listen en puerto 15000");

            // Loop aceptar cliente entrante, obtener e imprimir sus datos
            while(listen){
                client = serverSocket.accept();
                System.out.println("Ya se conecto un client desde: " +
                        client.getInetAddress().getHostName()+
                        "("+client.getPort()+")");
                new Attend(client).start();
            }

            // Termina la conexion establecida con el cliente
            serverSocket.close();

        }catch (IOException e){
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }
}

class Attend extends Thread{
    private BufferedReader bufferedReader;
    private DataOutputStream outputStream;
    private String recive;
    private Socket client = null;
    private String nameDirIP;

    // Obtener direccion IP del Socket cliente
    public Attend(Socket client){
        this.client = client;
        nameDirIP = this.client.getInetAddress().toString();
    }

    public void run(){
        try{
            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

            // Se obtiene el flujo entrante desde el cliente
            bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));

            // Flujo de datos hacia el servidor
            outputStream = new DataOutputStream(client.getOutputStream());

            // Leer buffer de entrada e imprimir IP + msj recibido mientras haya msj en el buffer
            do{
                recive = bufferedReader.readLine();
                System.out.println("("+ nameDirIP +") Llego: " + recive);
                if(recive!=null) {
                    outputStream.writeInt(recive.length());
                }
            }while(recive!= null && recive.length()!=0);

            // Cerrar buffer de entrada y cerrar Socket cliente
            bufferedReader.close();
            client.close();

        }catch(IOException e){
            System.out.println(e.getMessage());
            System.exit(1);
        }

        // Msj de desconexion
        System.out.println("Ya se desconecto"+"("+ nameDirIP +")");
    }
}
/*
 La clase server contiene el main del programa donde instancia el SocketServer con un puerto 15000, y reserva una
 variable socket para el potencial cliente.
 Una vez lanzado el server, este permanece en espera de un cliente. Al conectarse un cliente se procede a obtener sus
 datos de IP y puerto, para luego ser delegada a la clase Attend, que se encarga de obtener los flujos entrantes/salientes
 de datos Stream. Si el cliente envia un flujo de datos, Attend se encarga de imprimirla en pantalla, si el cliente finaliza
 el envio de datos, se procede a cerrar el buffer de entrada y  se llama al metodo del cliente de cierre.
 */