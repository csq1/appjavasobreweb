package Practica_1.Problemas;

// Declara dos metodos: foo() static y bar() las cuales imprimen un msj
class Parent{
    public static void foo(){System.out.println("I am foo in Test.Parent");}
    public void bar(){System.out.println("I am bar in Test.Parent");}
}

/*
Extiende (Herencia) de Parent por lo que hereda los mismos métodos,
los cuales imprimen una salida distinta.
 */
class Child extends Parent{
    public static void foo(){ System.out.println("I am foo in Test.Child");}
    public void bar(){ System.out.println("I am bar in Test.Child");}
}

/*
Clase de prueba se instancia dos objetos:
    1-	Objeto Child tipo Parent obj1.
    2-	Objeto Child tipo Child obj2
*/
class TestOutput {
    public static void main(String args[]){
        // El método que se invocará es determinado por el tipo de desde donde es invocado
        Parent obj1 = new Child();
        Child obj2 = new Child();


        // Como el metodo al ser static invoca el metodo de foo() del Parent.
        obj1.foo(); // I am foo in Parent

        // Como el metodo a invocar es static invoca su propio método foo().
        obj2.foo(); // I am foo in Child


        // Al no ser static invoca el método de la clase desde donde es invocado bar() osea Child
        obj1.bar(); // I am bar in Child

        // Invoca la salida del método de la clase invocadora, método bar() de la clase Child .
        obj2.bar(); // I am bar in Child


        // Retorna el texto esperado dado que se accede directamente al método de las clases
        Parent.foo(); // I am foo in Parent
        Child.foo(); // I am bar in Child
    }
}
