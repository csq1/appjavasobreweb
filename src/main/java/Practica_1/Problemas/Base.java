package Practica_1.Problemas;

//  Clase Base declara dos metodos: display() private y print() ambos imprimen un tipo msj
class Base{
    private static void display(){System.out.println("Static or class method from Base");}
    public void print(){System.out.println("Static or class method from Base");}
}


// Clase Derived hereda de Base sus metodos y los redefine
class Derived extends Base{
    public static void display(){System.out.println("Static or class method from Derived");}
    public void print(){ System.out.println("Static or class method from Derived"); }
}


class TestOverriding {
    public static void main(String args[]){

        // Instancia un objeto Derived tipo Base
        Base obj1 = new Derived();

        // display() no puede ser accedido por que la clase padre Base tiene el método display() como método privado.
        // Solucion: castear a Derived
        ((Derived) obj1).display(); // has private access | Salida: Static or class method from Derived

        // Salida: Static or class method from Derived
        obj1.print();
    }
}
