package Practica_1.Collections;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

class ConsumerSocket {
    Socket socket = null;
    PrintWriter printWriter;

    // Datos de conexion IP y puerto
    String HOST = "127.0.0.1";
    int PORT = 15000;

    public void init(){
        try{
            socket = new Socket(HOST,PORT);

            // Escribir texto e enviarlo en stream al socket
            printWriter = new PrintWriter(socket.getOutputStream(),true);

            // Iniciar la captura del teclado
            Scanner scanner = new Scanner(System.in);
            String read = "";

            do{
                read = scanner.nextLine();
                if(!read.equals("")) {
                    printWriter.println(read);
                }
            }while(!read.equals("EXIT"));

        }catch (IOException e){
            System.err.println("I/O error: " + e);
        }
    }

    // Metodo de finalizacion cierra socket
    public void stop(){
        try{
            socket.close();
        }catch (IOException e){
            System.err.println(e);
        }
    }
}

class Consumer{
    public static void main(String[] args){
        ArrayList<ConsumerSocket> consumers = new ArrayList<ConsumerSocket>();

        for (int i = 0; i < 5; i++) {
            consumers.add(new ConsumerSocket());
        }

        for (ConsumerSocket consumer : consumers) {
            consumer.init();
        }
    }
}
