package Practica_1.Collections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Service {
    public static void main(String[] args) {
        ServerSocket serviceSocket = null;
        Socket client = null;

        int PORT = 15000;

        try {
            serviceSocket = new ServerSocket(PORT);
            System.out.println("ServiceSocket listen en puerto " + PORT);

            while (true) {
                try {
                    client = serviceSocket.accept();

                    System.out.println("Se conecto un cliente desde: " +
                            client.getInetAddress().getHostName()+
                            "("+client.getPort()+")");

                } catch (IOException e) {
                    System.err.println("I/O error: " + e);
                }
                // Crear thread para el cliente
                new Support(client).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Support extends Thread{
    private Socket client = null;
    private String nameDirIP;
    private BufferedReader bufferedReader;
    private String recive;

    // Obtener direccion IP del Socket cliente
    public Support(Socket client){
        this.client = client;
        nameDirIP = this.client.getInetAddress().toString();
    }

    public void run(){
        try{
            Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

            // Se obtiene el flujo entrante desde el cliente
            bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));

            // Leer buffer de entrada e imprimir IP + msj recibido mientras haya msj en el buffer
            do{
                recive = bufferedReader.readLine();
                System.out.println("("+ nameDirIP +") Llego: " + recive);

                if(recive.equals("EXIT")) {
                    // Msj de desconexion
                    System.out.println("Se desconecto"+" ("+ nameDirIP +")"+"("+ client.getPort() +")");
                }

            }while(recive!= null && recive.length()!=0);

            // Cerrar buffer de entrada y cerrar Socket cliente
            bufferedReader.close();
            client.close();

        }catch(Exception e){
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }
}