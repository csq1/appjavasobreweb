package Practica_1.Collections;

import java.util.*;

public class Collections {

    public static void main(String[] args) {

        // Realizar una lista de compras con arraylist e imprimirla.
        ArrayList list = new ArrayList();
        list.add("Aceite de girasol");
        list.add("Arroz");
        list.add("Café");
        list.add("Vinagre");
        list.add("Yerba");
        list.add("Fideos");
        list.add("Azúcar");

        System.out.println("Lista de compras: ");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("---------------------------------------------------------------------------------------");

        // Realizar un catálogo de libros por isbn como clave, y recuperar e imprimir la información del mismo.
        Map<String, String> catalogo = new HashMap<String, String>();
        catalogo.put("978-950-614-0113","Cien años de soledad - García Márquez, Gabriel");
        catalogo.put("978-987-613-186-5","Cuentos de la selva - Quiroga, Horacio");
        catalogo.put("978-950-547-181-2","El señor de los anillos 1 - Tolkien, John Ronald Reuen");
        catalogo.put("978-987-3952-53-1","Cuentos completos - Poe, Edgar Allan");
        catalogo.put("978-987-725-371-9","Cuentos completos 1 - Cortázar, Julio");

        System.out.println("Libros del catalogo: ");

        for (String key: catalogo.keySet()) {
            System.out.println("ISBN : " + key +" | "+"Titulo/Autor: " + catalogo.get(key));
        }

        System.out.println();
        System.out.println("Info de libro con ISBN 978-950-547-181-2:");
        System.out.println(catalogo.get("978-950-547-181-2"));

        System.out.println("---------------------------------------------------------------------------------------");

        /*Realizar una impresión del ingreso de números de documento en una mesa de elecciones
        en un treeset, e imprimirla al final.*/

        Set treeSet = new TreeSet();

        System.out.println("Ingresos DNI: ");
        System.out.println("31486949");
        treeSet.add(31486949);
        System.out.println("28357396");
        treeSet.add(28357396);
        System.out.println("23629573");
        treeSet.add(23629573);
        System.out.println("30784274");
        treeSet.add(30784274);
        System.out.println("44839478");
        treeSet.add(44839478);

        System.out.println();
        System.out.println("Listado completo: ");
        System.out.println(treeSet);
    }
}